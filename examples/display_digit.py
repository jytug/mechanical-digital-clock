"""
Display a digit using 7 servos and a 16-channel Adafruit servo driver.

Fins on the digit are numbered as follows:

 00000
1     2
1     2
1     2
1     2
 33333
4     5
4     5
4     5
4     5
 66666
"""
from collections import defaultdict
import itertools
import json
import time

from adafruit_servokit import ServoKit

DIGIT_TO_FIN_STATE = {
    0: {
        0: True,
        1: True,
        2: True,
        3: False,
        4: True,
        5: True,
        6: True,
    },
    1: {
        0: False,
        1: False,
        2: True,
        3: False,
        4: False,
        5: True,
        6: False,
    },
    2: {
        0: True,
        1: False,
        2: True,
        3: True,
        4: True,
        5: False,
        6: True,
    },
    3: {
        0: True,
        1: False,
        2: True,
        3: True,
        4: False,
        5: True,
        6: True,
    },
    4: {
        0: False,
        1: True,
        2: True,
        3: True,
        4: False,
        5: True,
        6: False,
    },
    5: {
        0: True,
        1: True,
        2: False,
        3: True,
        4: False,
        5: True,
        6: True,
    },
    6: {
        0: True,
        1: True,
        2: False,
        3: True,
        4: True,
        5: True,
        6: True,
    },
    7: {
        0: True,
        1: False,
        2: True,
        3: False,
        4: False,
        5: True,
        6: False,
    },
    8: {
        0: True,
        1: True,
        2: True,
        3: True,
        4: True,
        5: True,
        6: True,
    },
    9: {
        0: True,
        1: True,
        2: True,
        3: True,
        4: False,
        5: True,
        6: True,
    },
}

FIN_TO_SERVO = dict(enumerate((0, 1, 2, 3, 8, 9, 10)))


class SingleDigitController:
    FINS = tuple(range(7))

    @classmethod
    def from_calibration_file(cls, filepath):
        with open(filepath, "r") as f:
            data = json.load(f)

        normalised_data = {
            int(fin): {
                is_up == "true": angle
                for is_up, angle in v.items()
            }
            for fin, v in data.items()
        }

        return cls(calibration=normalised_data)



    def __init__(self, calibration=None):
        self._servo_kit = ServoKit(channels=16)
        self._calibration = calibration

    def zero(self):
        print(f"Zeroing")

        for fin in self.FINS:
            self._servo_kit.servo[fin].angle = 0


    def calibrate(self):
        print(f"Beginning calibration process. For each fin and up/down position, please enter "
              f"a number between 0 and 180 (inclusive) which makes it look okay. When you're happy, enter 'ok'")
        calibration_data = defaultdict(dict)
        for fin, is_up in itertools.product(self.FINS, (False, True)):
            print(f"Fin {fin}, position {'UP' if is_up else 'DOWN'}")
            angle = 90
            servo = FIN_TO_SERVO[fin]

            while True:
                self._servo_kit.servo[servo].angle = angle
                data_from_user = input("> ")
                if data_from_user == "ok":
                    break
                else:
                    angle = int(data_from_user)
            calibration_data[fin][is_up] = angle

        print("Calibration data:")
        print(calibration_data)

        print("Writing to calibration.json")

        with open("calibration.json", "w") as f:
            json.dump(calibration_data, f)

    def _display_digit(self, digit):
        if not 0 <= digit <= 10:
            raise ValueError(f"Bad digit: {digit}")

        for fin in self.FINS:
            is_up = DIGIT_TO_FIN_STATE[digit][fin]
            angle = self._calibration[fin][is_up]
            servo = FIN_TO_SERVO[fin]
            self._servo_kit.servo[servo].angle = angle



    def count_up_forever(self):
        if self._calibration is None:
            raise ValueError("No calibration!")

        for digit_to_display in itertools.cycle(range(10)):
            self._display_digit(digit_to_display)
            time.sleep(2)


def main():
    controller = SingleDigitController.from_calibration_file("calibration.json")
    controller.count_up_forever()

if __name__ == "__main__":
    main()
